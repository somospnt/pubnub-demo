var app = {
    initialize: function () {
        this.bindEvents();
    },
    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);

    },
    onDeviceReady: function () {
        console.log('deviceready');
        app.main();
    },
    main: function () {

        var channel = 'pubnub-demo';
        var p = PUBNUB.init({subscribe_key: 'demo'});

        p.subscribe({
            channel: channel,
            callback: app.success
        });
    },
    success: function (m) {
        cordova.plugins.notification.local.schedule({
            id: 1,
            text: m.text,
            title: m.title,
            icon: 'icon',
            smallIcon: 'icon'
        });
    },
    onPause: function () {
        aletr(background);
        background = true;
    },
    onResume: function () {
        aletr(background);
        background = false;
    }
};

app.initialize();
