# pubnub-demo


### 1 Probar la aplicación utilizando cliente de Cordova

Clonar el repo, agregar platforms Andorid, agregar plugin de notificaciones, correrlo!

1. `git clone https://bitbucket.org/somospnt/pubnub-demo.git`
2. `cordova platform add android`
3. `cordova plugin add https://github.com/katzer/cordova-plugin-local-notifications`
4. `cordova run android`

### 2 Enviar mensaje desde la consola de PubNub

1. https://www.pubnub.com/console/
2. Configurar los siguientes valores
   channel=pubnub-demo - publish key=demo - subscribe key=demo
3. Presionar el botón "SUSCRIBE".
4. El mensaje es un json con dos atributos (text y title)
    Ejemplo: {"text":"Hola!!!", "title":"Mensaje"}
5. presionar el botón "send"
